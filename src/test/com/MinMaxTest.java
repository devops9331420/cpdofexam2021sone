package com.agiletestingalliance;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class MinMax {

    @Test
    public void testMinMax() throws Exception {
      MinMax minMax = mock(MinMax.class);
      
      String str = "HelloWorld";
      
      assertEquals(str, minMax.bar(str););
    }
    
}