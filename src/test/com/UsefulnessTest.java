package com.agiletestingalliance;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class UsefulnessTest {

    @Test
    public void testUsefulness() throws Exception {
      Usefulness usefulness = mock(Usefulness.class);
      
      String str = usefulness.desc();
      assertTrue(str.contains("DevOps is about transformation, about"));
    }
    
}